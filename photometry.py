# photometry.py

# Photometry
#
#  2023/07/12 H. Akitaya
#  2023/07/12 H. AKitaya; U-band appended.

from photutils.aperture import SkyCircularAperture, CircularAnnulus, CircularAperture, ApertureStats
from photutils.aperture import aperture_photometry

BAND_DATA = {"B": 0.44352, "V": 0.54914, "R": 0.65344, "I": 0.76945,
            "J": 1.2485, "H": 1.6380, "K": 2.1455, "Ks": 2.1455,
            "U": 0.364}


def get_wavelength_of_band(band, unit='um'):
    factor = 1.0
    if unit == 'A':
        factor = 10000.0
    if band in BAND_DATA:
        return BAND_DATA[band] * factor
    else:
        return 0.0


