#!/usr/bin/env

""" Photon transfer curve reduction.

2023/07/11 H. Akitaya
"""

import numpy as np
from scipy import optimize

import matplotlib.pyplot as plt


def get_sample_region(img_sig, img_dev, xmin, xmax, ymin, ymax):
    img = img_sig
    img_ysize = img.shape[0]
    img_xsize = img.shape[1]
    #img_xsize_per_ch = int(img_xsize / 4)
    #n_ch = n_port - 1
    #xmin = int(img_xsize_per_ch * n_ch + img_xsize_per_ch / 2 - sample_xsize_per_ch / 2)
    #xmax = int(img_xsize_per_ch * n_ch + img_xsize_per_ch / 2 + sample_xsize_per_ch / 2)
    print('img_sig.shape {}'.format(img_sig.shape))
    #print(xmin, xmax)
    array_sig = img_sig[ymin:ymax+1, xmin:xmax + 1].ravel()
    array_dev = img_dev[ymin:ymax+1, xmin:xmax + 1].ravel()
    return array_sig, array_dev


def calc_statistics_in_sample_region(array_sig, array_dev, count_bin_width=100):
    sig_all = []
    dev_all = []
    err_all = []
    count = 0
    count_max_calc = np.floor(np.max(array_sig) / count_bin_width - 1) * count_bin_width
    while count <= count_max_calc:
        # print(x)
        data_index = ((array_sig > count) & (array_sig <= count + count_bin_width))
        count += count_bin_width
        # print(data_index)
        # and array_sig <= (x+x_bin_width))
        sig_data_prt = array_sig[data_index]
        dev_data_prt = array_dev[data_index]

        trues = data_index[data_index]
        n_sample = len(trues)
        # print(n_sample)
        if n_sample == 0:
            continue
        sig = np.median(sig_data_prt)
        dev = np.median(dev_data_prt)
        error = np.std(dev_data_prt) / np.sqrt(n_sample - 1)
        # print(sig, dev, error, n_sample)
        sig_all.append(sig)
        dev_all.append(dev)
        err_all.append(error)
    return sig_all, dev_all, err_all


#def get_statistics(imgdata, x_bin_width, img_median, ):
#    array_sig = get_sample_regions(testimg_bs_median, n_port, img_xsize_per_ch, sample_xwidth_per_ch).ravel()
#    array_dev = get_sample_regions(testimg_bs_dev, n_port, img_xsize_per_ch, sample_xwidth_per_ch).ravel()
#    x_max_calc = np.floor(np.max(imgdata) / x_bin_width - 1) * x_bin_width


def photon_transfer_curve(x, gain, ro):
    return x / gain + (ro / gain) ** 2


def analyse_photon_transfer_curve(sig_all, dev_all, err_all, p0=(3, 10.0)):
    par, cov = optimize.curve_fit(photon_transfer_curve, sig_all, dev_all,
                                  sigma=err_all, p0=p0, absolute_sigma=False)
    return par, cov


def plt_photon_transfer_curve(sig_all, dev_all, err_all, par, x_bin_width=100):
    x_max_calc = np.floor(np.max(sig_all) / x_bin_width - 1) * x_bin_width
    x_fit = np.linspace(0, np.max(sig_all) * 0.8, 100)
    y_fit = photon_transfer_curve(x_fit, par[0], par[1])
    plt.grid()
    plt.xlabel('Count [ADU]')
    plt.ylabel('Deviation [ADU^2]')
    plt.xlim([0, x_max_calc])
    plt.ylim([0, 10000])
    plt.errorbar(sig_all, dev_all, yerr=err_all, fmt=".")
    plt.plot(x_fit, y_fit, color='r', linewidth=3)
    plt.show()


def calc_gain(img_median, img_dev, x1, x2, y1, y2):
    array_sig, array_dev = get_sample_region(img_median, img_dev,
                                             x1, x2, y1, y2)
    sig_all, dev_all, err_all = calc_statistics_in_sample_region(array_sig, array_dev)
    par, cov = analyse_photon_transfer_curve(sig_all, dev_all, err_all)
    plt_photon_transfer_curve(sig_all, dev_all, err_all, par)
    return par, cov


if __name__ == '__main__':
    pass
