from setuptools import setup

setup(
    name='kottamia_tools',
    version='1.0',
    packages=[''],
    url='',
    license='TBD',
    author='Hiroshi Akitaya',
    author_email='akitaya@perc.it-chiba.ac.jp',
    description='Kottamia/KFISP reduction tools'
)
