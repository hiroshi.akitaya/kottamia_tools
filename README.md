# kottamia_tools

Hiroshi Akitaya (PERC/CIT)

Since 2023-07-05

## Usage

+ Download repository.
$ git clone https://gitlab.com/hiroshi.akitaya/kottamia_tools.git
+ Import package in your python code as:
```python
import sys

KOTTAMIA_TOOLS_PATH = '/add/your/path/git_project/kottamia_tools/'
sys.path.append(KOTTAMIA_TOOLS_PATH)
```
+ Use methods and classes in the package.
```python
# (e.g.)
import redtools

fns_bias = redtools.select_images_by_header(fns, {'IM_STYLE': 'Bias'})
bias_data = redtool.imgs_combine(fns_bias, fn_out='bias_ave.fits')
20230708/20230708_0029.fits
20230708/20230708_0030.fits
20230708/20230708_0031.fits
20230708/20230708_0032.fits
20230708/20230708_0033.fits
20230708/20230708_0450.fits
20230708/20230708_0451.fits
20230708/20230708_0452.fits
20230708/20230708_0453.fits
20230708/20230708_0454.fits
20230708/20230708_0455.fits
20230708/20230708_0456.fits
20230708/20230708_0457.fits
20230708/20230708_0458.fits
20230708/20230708_0459.fits
20230708/20230708_0500.fits
Write to bias_ave.fits
```