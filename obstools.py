#!/usr/bin/env python

"""
Observation support tools.

2023/07/08 H. Akitaya

"""

import os

import datetime

import astroplan
from astropy.coordinates import SkyCoord, EarthLocation, AltAz
import astropy.units as u
from astroplan import Observer

# import pytz

__author__ = 'H. Akitaya'
__version__ = '1.0'

LATITUDE = 25.93167
LONGITUDE = 31.825
HEIGHT = 100.0
NAME = 'Kottamia'
TIMEZONE = 'Africa/Cairo'


def simg(fn):
    """ Show fits image on DS9 via XPA.
    """
    import pyds9

    targets = pyds9.ds9_targets()
    print(targets)
    d = pyds9.DS9('ds9')
    fn_full = os.path.realpath(fn)
    d.set(f'file {fn_full}')



def calc_visibility(time, sky_coord: SkyCoord, location: EarthLocation,
                    observer: astroplan.Observer):
    """

    :astroplan.Observer observer:
    """
    ha = observer.target_hour_angle(time, sky_coord)
    altaz = observer.altaz(time, sky_coord)

    alt_deg = altaz.alt.value
    az_deg = altaz.az.value
    ha_h = ha.value

    return ha_h, alt_deg, az_deg


if __name__ == '__main__':

    obj_coord = SkyCoord(210, 0, unit="deg")

    obj_coord = SkyCoord('14 03 12.583', '+54 20 55.50', unit=(u.h, u.deg))

    # Set location.
    location = EarthLocation(lat=LATITUDE * u.deg, lon=LONGITUDE * u.deg, height=HEIGHT * u.m)
    # Set observer.
    observer = Observer(location=location, name=NAME, timezone=TIMEZONE)

    dt_now = datetime.datetime.utcnow()
    lst = observer.local_sidereal_time(datetime.datetime.utcnow())

    print(lst)
    ha, alt, az = calc_visibility(dt_now, obj_coord, location, observer)
    print('{:.2f} {:.2f} {:.2f}'.format(ha, alt, az))
