#!/usr/bin/env python

""" Reduction support tools for Kottamia/KFISP.

2023/07/08 H. Akitaya
"""

import os
import sys

import numpy as np
from astropy.io import fits


def show_all_headers(fn):
    """ Show all fits header information.
    """
    with fits.open(fn) as hdul:
        for key in hdul[0].header:
            print(key, hdul[0].header[key])


def select_images_by_header(fns, cond_dict, logic='or'):
    """ Select filenames by fits header values.
    :param fns: file name list to be seached.
    :param cond_dict: Dictionary of a pair of header_key and value.
    :return: file name list.
    """
    fns_result = []
    for fn in fns:
        with fits.open(fn) as hdul:
            if logic == 'or':
                for key in cond_dict:
                    if hdul[0].header[key] == cond_dict[key]:
                        fns_result.append(fn)
            else:
                flag = True
                for key in cond_dict:
                    if hdul[0].header[key] != cond_dict[key]:
                        flag = False
                if flag:
                    fns_result.append(fn)
    return fns_result



def bias_subtraction(fn: str, fn_bias: str, overwrite=False, subext='_bs'):
    """ Bias subtraction.
    """
    with fits.open(fn) as hdul_img:
        with fits.open(fn_bias) as hdul_bias:
            fn_splt = os.path.splitext(fn)
            fn_new = fn_splt[0] + subext + fn_splt[1]
            hdul_img[0].data -= hdul_bias[0].data
            # Comments
            hdul_img[0].header['history'] = f'Bias subtraction: {fn} - {fn_bias} = {fn_new}'
            hdul_img.writeto(fn_new, overwrite=overwrite)
            return True
    return False


def flat_fielding(fn: str, fn_flat: str, overwrite=False, subext='_fl'):
    """ Flat fielding.
    """
    with fits.open(fn) as hdul_img:
        with fits.open(fn_flat) as hdul_flat:
            fn_splt = os.path.splitext(fn)
            fn_new = fn_splt[0] + subext + fn_splt[1]
            hdul_img[0].data /= hdul_flat[0].data
            # Comments
            hdul_img[0].header['history'] = f'Flat fielding: {fn_img} / {fn_flat} = {fn_new}'
            hdul_img.writeto(fn_new, overwrite=overwrite)
            return True


def imgs_combine(fns, fn_out=None, overwrite=True):
    """ Combine fits images.
    median mode.
    """
    imgs = []
    for fn in fns:
        with fits.open(fn) as hdul:
            imgs.append(hdul[0].data)
            print(fn)
    if len(imgs) < 1:
        sys.stderr.write('No images found.\n')
        sys.exit(1)
    imgs_ndarray = np.stack(imgs)
    del(imgs)
    imgs_comb = np.median(imgs_ndarray, axis=0)
    hdul[0].data = imgs_comb
    hdul[0].header['history'] = f'Combined: {str(fns)}'
    hdul[0].header['history'] = f'Combined file name: {fn}'
    if fn_out:
        hdul.writeto(fn_out, overwrite=overwrite)
    print(f'Write to {fn_out}')
    del(imgs_ndarray)
    return(hdul)


def create_flatimage(flat_fns, bias_fn, flat_fn=None, overwrite=True):
    """ Create flat image.
    :param flat_fns:
    :param bias_fn:
    :param flat_fn:
    :param overwrite:
    :return:
    """
    img_flat_comb_hdul = imgs_combine(flat_fns, fn_out=None)
    try:
        hdul_bias = fits.open(bias_fn)
    except IOError:
        sys.exit(1)
    img_flat_bs = img_flat_comb_hdul[0].data - hdul_bias[0].data
    flat_median_val = np.median(img_flat_bs)
    img_flat_nrm = img_flat_bs / flat_median_val
    img_flat_comb_hdul[0].data = img_flat_nrm
    img_flat_comb_hdul[0].header['history'] = f'Combined: {str(flat_fns)}'
    img_flat_comb_hdul[0].header['history'] = f'Bias subtracted: {bias_fn}'
    img_flat_comb_hdul[0].header['history'] = f'Normalized: {flat_median_val}'
    if flat_fn:
        img_flat_comb_hdul[0].header['history'] = f'File name: {flat_fn}'
        img_flat_comb_hdul.writeto(flat_fn, overwrite=overwrite)
        print(f'Write to {flat_fn}')
    hdul_bias.close()
    return img_flat_comb_hdul


def add_honirred_headers(fn):
    """
    Add fits headers for compatibility to HONIRRED.
    :param fn:
    :return:
    """
    with fits.open(fn) as hdul:
        hdul[0].header['HN-ARM'] = 'opt'
        hdul[0].header['WH_OPTF2'] = hdul[0].header['FILTER']
        hdul[0].header['HWPANGLE'] = hdul[0].header['HALFPOS']
        hdul[0].header['HA-ANG'] = 0.0
        hdul[0].header['MJD'] = 0.0
        hdul[0].header['AIRMASS'] = float(hdul[0].header['SEC_Z'])
        hdul.writeto(fn, overwrite=True)

