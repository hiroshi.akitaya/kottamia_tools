#!/usr/bin/env python

"""
Calculate offset coordinate.
2023-07-05 H. Akitaya

(e.g)
./calc_offset_coord.py '02 04 01' '+0 0 0' 15 0
02h04m02s +00d00m00s
"""

__author__ = 'H. Akitaya'
__version__ = '1.0'


import sys

import numpy as np

from astropy.coordinates import SkyCoord
import astropy.units as u

def calc_offset_radec(ra_str, dec_str, dra, ddec):
    """ Calculate offset coordinate.
    ra_str: RA in string (e.g. "04 42 56.1")
    dec_str: Dec in string (e.g. "+23 44 12.3")
    dra: ra offset (arcsec)
    ddec: dec offset (arcsec)
    """
    coord = SkyCoord(ra_str, dec_str, unit=(u.hourangle, u.deg), frame='icrs')
    separation = np.sqrt(dra**2 + ddec**2) * u.arcsec
    position_angle = np.rad2deg(np.arctan2(dra, ddec)) * u.deg
    coord_offset = coord.directional_offset_by(position_angle, separation)
    return coord_offset

if __name__ == '__main__':
    
    # Argument parser
    import argparse

    parser = argparse.ArgumentParser(prog='calc_offset_coord')
    parser.add_argument('ra_str', type=str, help='RA in string')
    parser.add_argument('dec_str', type=str, help='DEC in string')
    parser.add_argument('dra', type=float, help='RA offset (arcsec)')
    parser.add_argument('ddec', type=float, help='DEC offset (arcsec)')
    
    args = parser.parse_args()

    coord_offset = calc_offset_radec(args.ra_str, args.dec_str,
                                     args.dra, args.ddec)
    print(coord_offset.to_string('hmsdms'))
    
    

    
    
    

